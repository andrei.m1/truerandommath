﻿using System;
using TrueRandomMath;

namespace RandomizerExample
{
    public static class Generator
    {
		/// <summary>
		/// Returns a nonnegative random integer that is less than the specified maximum.
		/// </summary>
		/// <param name="maxValue">The exclusive upper bound of the random number to be generated.
		/// maxValue must be greater than or equal to zero.</param>
		/// <returns>A 32-bit signed integer greater than or equal to zero, and less than maxValue;
		/// that is, the range of return values ordinarily includes zero but not maxValue.
		/// However, if maxValue equals zero, maxValue is returned.</returns>
		public static int Next(int maxValue = int.MaxValue)
		{
			if (maxValue - 1 < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(maxValue));
			}

			return Next(0, maxValue);
		}

		/// <summary>
		/// Returns a random integer that is within a specified range.
		/// </summary>
		/// <param name="minValue">The inclusive lower bound of the random number returned.</param>
		/// <param name="maxValue">The exclusive upper bound of the random number returned.
		/// maxValue must be greater than or equal to minValue.</param>
		/// <returns>A 32-bit signed integer greater than or equal to minValue and less than maxValue;
		/// that is, the range of return values includes minValue but not maxValue. If minValue
		/// equals maxValue, minValue is returned.</returns>
		public static int Next(int minValue, int maxValue)
		{
			return RandomMath.GetRealRandomInt(minValue, maxValue - 1);
		}

		/// <summary>
		/// Returns a random number between 0.0 and 1.0.
		/// </summary>
		/// <returns>
		/// A double-precision floating point number greater than or equal to 0.0, and less than 1.0.
		/// </returns>
		public static double NextDouble()
		{
			return RandomMath.GetRandomValueReal();
		}
    }
}