﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace RandomizerExample
{
    public class Program
    {
        static void Main(string[] _)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("Welcome to the example of using TrueRandomMath!");
            Thread.Sleep(5000);
            Console.WriteLine("Let's get started...");
            Thread.Sleep(1000);
            Console.WriteLine("5");
            Thread.Sleep(1000);
            Console.WriteLine("4");
            Thread.Sleep(1000);
            Console.WriteLine("3");
            Thread.Sleep(1000);
            Console.WriteLine("2");
            Thread.Sleep(1000);
            Console.WriteLine("1");
            Console.WriteLine("GO!");
            Console.WriteLine();
            List<int> results = new ();
            for (var i = 0; i < 100000; i++)
            {
                results.Add(Generator.Next(10));
            }

            foreach (var result in results.Distinct())
            {
                Console.WriteLine($"result: {result}, percent: {results.Count(e => e == result) / (decimal)results.Count}");
            }

            Console.WriteLine();
            Console.WriteLine("The END.");
        }
    }
}