﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TrueRandomMath
{
    /// <summary>
    /// Random mathematics class
    /// </summary>
    public static class RandomMath
    {
        private static readonly Sha2PrngProvider InnerSha256 = new Sha2PrngProvider();

        /// <summary>
        /// Returns the truly real random value between 0 and 1
        /// </summary>
        /// <returns>Random number</returns>
        public static double GetRandomValueReal()
        {
            return InnerSha256.Next();
        }

        private class ObjAndDouble<T>
        {
            public T Obj;
            public double Dbl;
        }

        /// <summary>
        /// Collection random sorting extension
        /// </summary>
        /// <typeparam name="T"> Collection type </typeparam>
        /// <param name="collection"> Collection </param>
        /// <returns> Random sorted collection </returns>
        public static IEnumerable<T> RandomSort<T>(this IEnumerable<T> collection)
        {
            if(collection == null)
                return Array.Empty<T>();

            var array = collection as T[] ?? collection.ToArray();
            if (!array.Any())
                return Array.Empty<T>();

            var collectionResults = array.Select(o => new ObjAndDouble<T>
                {
                    Obj = o, 
                    Dbl = GetRandomValueReal()
                })
                .OrderBy(o => o.Dbl)
                .Select(o => o.Obj);
            return collectionResults;
        }

        /// <summary>
        /// Returns the array of the truly real random values  between 0 and 1
        /// </summary>
        /// <param name="numberOfRandoms"> Array size </param>
        /// <returns></returns>
        public static double[] GetRandomValuesReal(int numberOfRandoms)
        {
            if (numberOfRandoms <= 0)
                return Array.Empty<double>();

            var res = new double[numberOfRandoms];
            for (var i = 0; i < numberOfRandoms; i++)
            {
                res[i] = GetRandomValueReal();
            }

            return res;
        }

        /// <summary>
        /// Returns the truly int random value between bottom and top values inclusive (advanced)
        /// </summary>
        /// <param name="bottom"> Bottom value </param>
        /// <param name="top"> Top value </param>
        /// <returns> Random integer value </returns>
        public static int GetRealRandomInt(int bottom, int top)
        {
            double val = InnerSha256.Next();
            double number = val * (top + 1 - bottom);
            var result = (int)number + bottom;
            return result;
        }

        /// <summary>
        /// Returns the array of the truly int random values between bottom and top values inclusive
        /// </summary>
        /// <param name="bottom"> Bottom value </param>
        /// <param name="top"> Top value </param>
        /// <param name="num"> Array size </param>
        /// <returns> Array with the int random values </returns>
        public static int[] GetRealRandomIntArray(int bottom, int top, int num)
        {
            var result = new int[num];
            for (var i = 0; i < num; i++)
                result[i] = GetRealRandomInt(bottom, top);

            return result;
        }

        /// <summary>
        /// Returns the Sum value of a few random int values
        /// </summary>
        /// <param name="bottom"> Bottom value </param>
        /// <param name="top"> Top value </param>
        /// <param name="num"> Array size </param>
        /// <returns> Random int value </returns>
        public static long GetRealRandomSum(int bottom, int top, int num)
        {
            return GetRealRandomIntArray(bottom, top, num).Sum();
        }

        /// <summary>
        /// Returns the truly long random value between bottom and top values inclusive
        /// </summary>
        /// <param name="bottom"> Bottom value </param>
        /// <param name="top"> Top value </param>
        /// <returns> Random long value </returns>
        public static long GetRealRandomInt(long bottom, long top)
        {
            double val = InnerSha256.Next();
            double number = val * (top + 1 - bottom);
            var result = (long)number + bottom;
            return result;
        }
    }
}