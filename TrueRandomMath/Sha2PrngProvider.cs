﻿using System;
using System.Security.Cryptography;

namespace TrueRandomMath
{
    public class Sha2PrngProvider
    {
        private readonly long _seed = DateTime.UtcNow.Ticks;
        private long _nonce;
        private readonly SHA256 _sha256 = SHA256.Create();

        private readonly object _sync = new object();

        public Sha2PrngProvider()
        {
            var dummy = string.Empty;
            _seed += dummy.GetHashCode();
        }

        public double Next()
        {
            lock (_sync)
            {
                var bytes = BitConverter.GetBytes(_seed + _nonce);
                var hashValue = _sha256.ComputeHash(bytes);
                var rndUInt = BitConverter.ToUInt64(hashValue, 0);
                var val = Convert.ToDouble(rndUInt >> 11) * 1.1102230246251565E-16;
                _nonce++;
                return val;
            }
        }
    }
}